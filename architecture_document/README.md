# GAIA-X Architecture Document

## Publisher

GAIA-X, European Association for Data and Cloud, AISBL  
Rue Royale 94  
1000 Bruxelles  
www.gaia-x.eu

## Authors

Gaia-X Technical Committee  
Gaia-X Work Packages  
Gaia-X Working Group Architecture  
Gaia-X Working Group Federation Services / Open Source Software  
Gaia-X Working Group Portfolio  
Gaia-X Working Group Provider  
Gaia-X Working Group User

## Contact

Email: architecture-document@gaia-x.eu

## Copyright notice

©2021 Gaia-X, European Association for Data and Cloud, AISBL

This document is protected by copyright law and international treaties. You may download, print or electronically view this document for your personal
or internal company (or company equivalent) use. You are not permitted to adapt, modify, republish, print, download, post or otherwise reproduce or
transmit this document, or any part of it, for a commercial purpose without the prior written permission of Gaia-X, European Association for Data and
Cloud, AISBL. No copying, distribution, or use other than as expressly provided herein is authorized by implication, estoppel or otherwise. All rights not
expressly granted are reserved.

Third party material or references are cited in this document.

 <script src="https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js"></script>
 <script>
mermaid.initialize({
    startOnLoad: true,
    mermaid: {
        callback: function(id) {
            console.log("mermaid callback with id", id);
            let s = document.querySelector('#' + id);
            let x = new XMLSerializer().serializeToString(s);
            let b = window.btoa(x);
            s.parentNode.innerHTML = "<img src='data:image/svg+xml;base64, " + b + "'/>";
            console.log(s.parentNode.innerHTML);
        }
    }
});</script>
<script>
// https://mybyways.com/blog/convert-svg-to-png-using-your-browser

function svg2png(id) {
    console.log("mermaid callback with id", id);
    var svg = document.querySelector('#' + id);
    var canvas = document.createElement('canvas');
    svg.parentNode.appendChild(canvas);
    canvas.height = svg.getBoundingClientRect().height;
    canvas.width = svg.getBoundingClientRect().width;
    var data = new XMLSerializer().serializeToString(svg);
    var win = window.URL || window.webkitURL || window;
    var img = new Image();
    var blob = new Blob([data], { type: 'image/svg+xml' });
    var url = win.createObjectURL(blob);
    img.onload = function () {
        console.log("img.onload");
        canvas.getContext('2d').drawImage(img, 0, 0);
        console.log("drawimage");
        win.revokeObjectURL(url);
        console.log("revokeURL(url)");
        var uri = canvas.toDataURL('image/png').replace('image/png', 'octet/stream');
        console.log("toDataUrl");
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.style = 'display: none';
        a.href = uri
        a.download = (svg.id || svg.svg.getAttribute('name') || svg.getAttribute('aria-label') || 'untitled') + '.png';
        a.click();
        console.log("a.click");
        window.URL.revokeObjectURL(uri);
        console.log("revokeurl(uri)");
        document.body.removeChild(a);
        svg.remove();
        console.log("svg.remove");
    };
    img.src = url;
    svg.parentNode.appendChild(img);
}
// window.onload = function() {
//     mermaid.initialize({
//         startOnLoad: true,
//         mermaid: {
//             callback: function(id) {
//                 console.log("mermaid callback with id", id);
//                 var svg = document.querySelector('#' + id);
//                 var canvas = document.createElement('canvas');
//                 svg.parentNode.appendChild(canvas);
//                 canvas.height = svg.getBoundingClientRect().height;
//                 canvas.width = svg.getBoundingClientRect().width;
//                 var data = new XMLSerializer().serializeToString(svg);
//                 var win = window.URL || window.webkitURL || window;
//                 var img = new Image();
//                 var blob = new Blob([data], { type: 'image/svg+xml' });
//                 var url = win.createObjectURL(blob);
//                 img.onload = function () {
//                     console.log("img.onload");
//                     canvas.getContext('2d').drawImage(img, 0, 0);
//                     console.log("drawimage");
//                     win.revokeObjectURL(url);
//                     console.log("revokeURL(url)");
//                     var uri = canvas.toDataURL('image/png').replace('image/png', 'octet/stream');
//                     console.log("toDataUrl");
//                     var a = document.createElement('a');
//                     document.body.appendChild(a);
//                     a.style = 'display: none';
//                     a.href = uri
//                     a.download = (svg.id || svg.svg.getAttribute('name') || svg.getAttribute('aria-label') || 'untitled') + '.png';
//                     a.click();
//                     console.log("a.click");
//                     window.URL.revokeObjectURL(uri);
//                     console.log("revokeurl(uri)");
//                     document.body.removeChild(a);
//                     svg.remove();
//                     console.log("svg.remove");
//                 };
//                 img.src = url;
//                 svg.parentNode.appendChild(img);
//             }
//         }
//     });
// });
</script>
