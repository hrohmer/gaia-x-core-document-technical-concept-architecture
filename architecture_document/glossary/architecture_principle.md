## Architecture Principle

Architecture Principles define the underlying guidelines for the use and deployment of all IT resources and assets across the initiative.
They reflect a level of consensus among the various elements of the initiative and form the basis for making future IT decisions.

### references
- Adapted from Togaf V 9.2, 20.2

