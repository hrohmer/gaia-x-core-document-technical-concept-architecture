## Provider

A [Participant](#participant) who provides [Assets](#asset) and [Resources](#resource) in the GAIA-X ecosystem.
