## Asset Owner

A natural or legal person who is in legal possession of the [Asset] and is responsible to set policy rules on the [Asset].
