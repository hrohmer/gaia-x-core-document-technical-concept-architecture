## Consumer

A Consumer is a [Participant](#participant) who consumes [Service Instance](#service-instance) in the GAIA-X ecosystem to enable digital offerings for [End Users](#end-user)
