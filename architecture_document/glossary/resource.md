## Resource

Internal, not available for order, [Service Instance](#service-instance) used to compose the [Service Offering](#service-offering), which exposes endpoints.

Resource prominent attributes are the location - physical address, Autonomous System Number, network segment - and the jurisdiction affiliations.
