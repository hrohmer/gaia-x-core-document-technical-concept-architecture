# GAIA-X Architecture Documentation

This repository contains the architecture documentation for GAIA-X.

The repository is organized into the following folders:

## Architecture Document

The Architecture Document describes the current common understanding of the Architecture.

The output is published here <https://gaia-x.gitlab.io/gaia-x-technical-committee/gaia-x-core-document-technical-concept-architecture/>

### Local development

1. Create a python3 virtual environment
2. `pip install -r requirements.txt`
3. `mkdocs serve -a 0.0.0.0:8000`
4. `mkdocs build`

Generated files are in `public/`

### Local development with docker

1. Setup docker SE on your local computer
2. `docker build -t gaia-x-mkdocs .`
3. `docker run --rm -it -p 8000:8000 -v ${PWD}:/docs gaia-x-mkdocs`
4. Open your browser with <http://localhost:8000/>
5. Start editing and review the changes live in your browser

## Architecture Decision Records

Architecture Decision Records (ADR) document important decisions that were proposed and accepted for GAIA-X.

The ADR are listed in in appendix of the Architecture Document.

